<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlbumRequest;
use App\Models\Album;
use App\Models\Song;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlbumController extends BackendBaseController
{
    protected $panel = 'Album';
    protected  $base_path = 'backend.album.';
    protected  $base_route = 'backend.album.';

    public function  __construct(){
        $this->model = new Album();
    }


    public function index()
    {
        $data['records'] = $this->model->all();
        return view($this->__loadDataToView($this->base_path . 'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['songs'] = Song::pluck('song_name','id');
        return view( $this->__loadDataToView($this->base_path . 'create'),compact('data'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlbumRequest $request)
    {
        $request->request->add(['created_by' => Auth::user()->id]);
        $album = $this->model->create($request->all());
        if ($album) {
            $album->songs()->attach($request->song_id);

            $request->session()->flash('success', $this->panel . ' created successfully');
        }else {
            $request->session()->flash('error', $this->panel . ' creation failed');
        }
        return redirect()->route($this->base_route . 'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        return  view($this->__loadDataToView($this->base_path . 'show'),compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['songs'] = Song::pluck('song_name','id');
        $data['record'] = $this->model->findOrFail($id);
        $songs = $data['record']->songs()->get();
        $data['assigned_song']  = [];
        foreach ($songs  as $song){
            $data['assigned_song'][] = $song->id;
        }
        return  view($this->__loadDataToView($this->base_path . 'edit'),compact('data'));
    }

    public function update(AlbumRequest $request, $id)
    {
        $data['record'] = $this->model->findOrFail($id);

        $request->request->add(['updated_by' => Auth::user()->id]);

        $album = $data['record']->update($request->all());
        if ($album) {

            $data['record']->songs()->sync($request->song_id);
            $request->session()->flash('success',$this->panel . ' updated successfully');
        } else {
            $request->session()->flash('error',$this->panel . ' update failed');
        }
        return redirect()->route($this->base_route . 'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['record'] = $this->model->findOrFail($id);
        if ($data['record']->delete()){
            request()->session()->flash('success',$this->panel . ' deleted successfully');
        } else {
            request()->session()->flash('error',$this->panel . ' delete failed');
        }
        return redirect()->route($this->base_route . 'index');
    }
    public function trash()
    {
        $data['records'] = $this->model->onlyTrashed()->get();
        return view($this->__loadDataToView($this->base_path . 'trash'),compact('data'));
    }

    public function restore($id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->restore();
        request()->session()->flash('success',$this->panel . ' restored');

        return redirect()->route($this->base_route . 'trash');
    }

    public function forceDelete(Request  $request,$id)
    {
        $data['record'] = $this->model->onlyTrashed()->where('id',$id)->forceDelete();
        request()->session()->flash('success',$this->panel . ' deleted successfully');
        return redirect()->route($this->base_route . 'trash');
    }
}
