<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Playlist;
use App\Models\Song;
use Illuminate\Http\Request;

class FrontendBaseController extends Controller
{
    protected  function  __loadDataToView($viewPath){
        view()->composer($viewPath, function ($view) {
            $albums = Album::orderby('name')->get();
            $view->with('albums', $albums);
            $playlists=Playlist::all();
            $view->with('playlists',$playlists);
            $songs=Song::orderby('song_name')->get();
            $view->with('songs',$songs);
            $view->with('base_path', $this->base_path);
            $view->with('base_route', $this->base_route);
        });
        return $viewPath;
    }
}







