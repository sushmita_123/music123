<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Playlist;
use App\Models\Song;
use Illuminate\Http\Request;

class FrontController extends FrontendBaseController
{
    protected  $base_path = 'frontend.front.';
    protected  $base_route = 'frontend.front.';

    public function index()
    {
        return view( $this->__loadDataToView($this->base_path . 'index'));
    }

    public function album()
    {
        $data['albums'] = Album::orderby('name')->get();
        return view( $this->__loadDataToView($this->base_path . 'album'),compact('data'));
    }
    public function playlist()
    {
        $data['playlist'] = Playlist::all();


        return view( $this->__loadDataToView($this->base_path . 'playlist'),compact('data'));
    }
    public function song()
    {
        $data['song'] = Song::orderby('song_name')->get();
        return view( $this->__loadDataToView($this->base_path . 'song'),compact('data'));
    }

}
