<?php

namespace App\Http\Controllers;
use Auth;

use Illuminate\Http\Request;

class Dashboardcontroller extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }
    function index()
    {
        return view('backend.dashboard.index');
    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }

}
