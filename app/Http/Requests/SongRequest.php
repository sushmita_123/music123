<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SongRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'song_name' => 'required',
            'title'=>'required',
            'duration'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'song_name.required' => 'Please enter song name',
            'title.required' => 'Please enter title',
            'duration.required'=>'enter duration'

        ];
    }

}
