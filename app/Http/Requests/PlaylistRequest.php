<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaylistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'playlist_name' => 'required',
            'playlist_date'=>'required',
            'duration'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'playlist_name.required' => 'Please enter song name',
            'playlist_date.required' => 'Please enter genre',
            'duration.required'=>'enter year'

        ];
    }

}
