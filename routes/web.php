<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\SongController;
use App\Http\Controllers\Backend\ArtistController;
use App\Http\Controllers\Backend\AlbumController;
use App\Http\Controllers\Backend\PlaylistController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\ClientController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
////    return view('welcome');
//    return view('frontend.index');
//});
Route::get('/', [\App\Http\Controllers\Frontend\FrontController::class, 'index'])->name('frontend.front.index');
Route::get('/album/', [\App\Http\Controllers\Frontend\FrontController::class, 'album'])->name('frontend.front.album');
Route::get('/playlist/', [\App\Http\Controllers\Frontend\FrontController::class, 'playlist'])->name('frontend.front.playlist');
Route::get('/song/', [\App\Http\Controllers\Frontend\FrontController::class, 'song'])->name('frontend.front.song');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\DashboardController::class, 'index'])->name('home');
Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile.index');
Route::get('logout', '\App\Http\Controllers\DashboardController@logout');
Route::get('/logout', [App\Http\Controllers\DashboardController::class, 'logout'])->name('logout');



Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/artist/trash', [ArtistController::class, 'trash'])->name('artist.trash');
    Route::get('/artist/restore/{id}', [ArtistController::class, 'restore'])->name('artist.restore');
    Route::delete('/artist/force-delete/{id}', [ArtistController::class, 'forceDelete'])->name('artist.force_delete');
    Route::resource('artist', ArtistController::class);
});


Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/song/trash', [SongController::class, 'trash'])->name('song.trash');
    Route::get('/song/restore/{id}', [SongController::class, 'restore'])->name('song.restore');
    Route::delete('/song/force-delete/{id}', [SongController::class, 'forceDelete'])->name('song.force_delete');
    Route::resource('song', SongController::class);
});


Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/album/trash', [AlbumController::class, 'trash'])->name('album.trash');
    Route::get('/album/restore/{id}', [AlbumController::class, 'restore'])->name('album.restore');
    Route::delete('/album/force-delete/{id}', [AlbumController::class, 'forceDelete'])->name('album.force_delete');
    Route::resource('album', AlbumController::class);
});

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/playlist/trash', [PlaylistController::class, 'trash'])->name('playlist.trash');
    Route::get('/playlist/restore/{id}', [PlaylistController::class, 'restore'])->name('playlist.restore');
    Route::delete('/playlist/force-delete/{id}', [PlaylistController::class, 'forceDelete'])->name('playlist.force_delete');
    Route::resource('playlist', PlaylistController::class);
});

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/user/trash', [UserController::class, 'trash'])->name('user.trash');
    Route::get('/user/restore/{id}', [UserController::class, 'restore'])->name('user.restore');
    Route::delete('/user/force-delete/{id}', [UserController::class, 'forceDelete'])->name('user.force_delete');

    Route::resource('user', UserController::class);
});

Route::prefix('backend')->name('backend.')->middleware('auth')->group(function () {
    Route::get('/client/trash', [ClientController::class, 'trash'])->name('client.trash');
    Route::get('/client/restore/{id}', [ClientController::class, 'restore'])->name('client.restore');
    Route::delete('/client/force-delete/{id}', [ClientController::class, 'forceDelete'])->name('client.force_delete');
    Route::resource('client', ClientController::class);
});
