

@include('frontend.includes.header')

<body>
<!-- header section -->
<section class="banner" role="banner" id="banner">

    <header id="header">
        <div class="header-content clearfix"> <span class="logo"><a href="index.html">Musicfy</a></span>
            @include('frontend.includes.menu')
            <a href="#" class="nav-toggle">Menu<span></span></a> </div>
    </header>

    <!-- banner text -->
    <div class="container">
        <div class="col-md-10">
            <div class="banner-text text-center">
                <h1>Music Play</h1>
                <p>Hello Welcome To Musicfy</p></div>
            <!-- banner text -->
        </div>
    </div>
</section>
<section id="intro" class="section intro">
    <div class="container">
        <div class="col-md-8 col-md-offset-2 text-center">
            <h3>Best Music Bootstrap Template</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu libero scelerisque ligula sagittis faucibus eget quis lacus.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            <a href="#Playlist" class="btn btn-large">Playlist</a>
        </div>
    </div>
</section>
<!-- intro section -->
<!-- services section -->

<!-- services section -->







<!-- our team section -->
<section id="teams" class="section teams">
    <div class="container">
        <div class="section-header">
            <h2 class="wow fadeInDown animated">Our Gang</h2>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="person"><img src="{{asset('demos/frontend/images/sushmita.png')}}" alt="" class="img-responsive">
                    <div class="person-content">
                        <h4>Sushmita Dahal</h4>
                        <h5 class="role">Admin</h5>
                        <p>Feel the music.....</p>
                    </div>
                    <ul class="social-icons clearfix">
                        <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="person"> <img src="{{asset('demos/frontend/images/team-1.jpg')}}" alt="" class="img-responsive">
                    <div class="person-content">
                        <h4>Selena</h4>
                        <h5 class="role">Singer</h5>
                        <p>Sing along with me</p>
                    </div>
                    <ul class="social-icons clearfix">
                        <li><a href="#" class=""><span class="fa fa-facebook"></span></a></li>
                        <li><a href="#" class=""><span class="fa fa-twitter"></span></a></li>
                        <li><a href="#" class=""><span class="fa fa-google-plus"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="person"> <img src="{{asset('demos/frontend/images/prasede.png')}}" alt="" class="img-responsive">
                    <div class="person-content">
                        <h4>Prasiddhi Poudel</h4>
                        <h5 class="role">Admin</h5>
                        <p>Let the music remove your sadness</p>
                    </div>
                    <ul class="social-icons clearfix">
                        <li><a href="#" class=""><span class="fa fa-facebook"></span></a></li>
                        <li><a href="#" class=""><span class="fa fa-twitter"></span></a></li>
                        <li><a href="#" class=""><span class="fa fa-google-plus"></span></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="person"> <img src="{{asset('demos/frontend/images/aakriti.png')}}" alt="" class="img-responsive">
                    <div class="person-content">
                        <h4>Aakriti Poudel</h4>
                        <h5 class="role">Admin</h5>
                        <p>Music for your every mood...</p>
                    </div>
                    <ul class="social-icons clearfix">
                        <li><a href="#" class=""><span class="fa fa-facebook"></span></a></li>
                        <li><a href="#" class=""><span class="fa fa-twitter"></span></a></li>
                        <li><a href="#" class=""><span class="fa fa-google-plus"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- our team section -->
<!-- Testimonials section -->
<section id="testimonials" class="section testimonials no-padding">
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="col-md-12">
                            <blockquote>
                                <h1>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget risus vitae massa
                                    semper aliquam quis mattis consectetur adipiscing elit.." </h1>
                                <p>Chris Mentsl</p>
                            </blockquote>
                        </div>
                    </li>
                    <li>
                        <div class="col-md-12">
                            <blockquote>
                                <h1>"Praesent eget risus vitae massa Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget risus vitae massa
                                    semper aliquam quis mattis consectetur adipiscing elit.." </h1>
                                <p>Kristean velnly</p>
                            </blockquote>
                        </div>
                    </li>
                    <li>
                        <div class="col-md-12">
                            <blockquote>
                                <h1>"Consectetur adipiscing elit Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget risus vitae massa
                                    semper aliquam quis mattis consectetur adipiscing elit.." </h1>
                                <p>Markus Denny</p>
                            </blockquote>
                        </div>
                    </li>
                    <li>
                        <div class="col-md-12">
                            <blockquote>
                                <h1>"Vitae massa semper aliquam Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget risus vitae massa
                                    semper aliquam quis mattis consectetur adipiscing elit.." </h1>
                                <p>John Doe</p>
                            </blockquote>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Testimonials section -->

<!-- contact section -->

<!-- contact section -->
@include('frontend.includes.footer')



