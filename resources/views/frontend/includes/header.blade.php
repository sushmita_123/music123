<!doctype html>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="">
<html class="no-js lt-ie9 lt-ie8" lang="">
<html class="no-js lt-ie9" lang="">
<html class="no-js" lang="">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="webthemez.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Music System</title>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('demos/frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('demos/frontend/css/flexslider.css')}}">
    <link rel="stylesheet" href="{{asset('demos/frontend/css/jquery.fancybox.css')}}">
    <link rel="stylesheet" href="{{asset('demos/frontend/css/main.css')}}">
    <link href="{{asset('demos/frontend/jPlayer-2.9.2/dist/skin/pink.flag/css/jplayer.pink.flag.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('demos/frontend/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('demos/frontend/css/font-icon.css')}}">
    <link rel="stylesheet" href="{{asset('demos/frontend/css/animate.min.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    @yield('css')
</head>



<!-- header section -->
<body>
<!-- header section -->
