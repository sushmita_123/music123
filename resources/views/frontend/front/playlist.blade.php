@include('frontend.includes.header')

<header id="header">
    <div class="header-content clearfix"> <span class="logo"><a href="index.html">Musicfy</a></span>
        @include('frontend.includes.menu')
        <a href="#" class="nav-toggle">Menu<span></span></a> </div>
</header>

<div class="abc">
    <img src="{{asset('demos/frontend/images/song.jpg')}}" alt="album" class="albumbladephp">
</div>

{{--<head>--}}
{{--    <link href="{{asset('demos/frontend/jPlayer-2.9.2/dist/skin/pink.flag/css/jplayer.pink.flag.min.css')}}" rel="stylesheet" type="text/css" />--}}
{{--</head>--}}
<section>
    <div class="container">
        <div class="section-header">
            <h2 class="wow fadeInDown animated">Playlists</h2>
        </div>
{{--        <div class="row">--}}
{{--            <div id="jquery_jplayer_2" class="jp-jplayer"></div>--}}

{{--            <div id="jp_container_2" class="jp-audio" role="application" aria-label="media player">--}}
{{--                <div class="jp-type-playlist">--}}

{{--                    <div class="jp-gui jp-interface">--}}
{{--                        <div class="jp-volume-controls">--}}
{{--                            <button class="jp-mute" role="button" tabindex="0">mute</button>--}}
{{--                            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>--}}
{{--                            <div class="jp-volume-bar">--}}
{{--                                <div class="jp-volume-bar-value"></div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="jp-controls-holder">--}}
{{--                            <div class="jp-controls">--}}
{{--                                <button class="jp-previous" role="button" tabindex="0">previous</button>--}}
{{--                                <button class="jp-play" role="button" tabindex="0">play</button>--}}
{{--                                <button class="jp-stop" role="button" tabindex="0">stop</button>--}}
{{--                                <button class="jp-next" role="button" tabindex="0">next</button>--}}
{{--                            </div>--}}
{{--                            <div class="jp-progress">--}}
{{--                                <div class="jp-seek-bar">--}}
{{--                                    <div class="jp-play-bar"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>--}}
{{--                            <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>--}}
{{--                            <div class="jp-toggles">--}}
{{--                                <button class="jp-repeat" role="button" tabindex="0">repeat</button>--}}
{{--                                <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}



{{--                                    <div class="jpx-playlist">--}}
                                        @foreach($playlists as $playlist)
{{--                                            <ul class="list-default">--}}
                        <div class="playlist-list">
                            <div class="playlist-name">
                        <a href="" >{{ucwords($playlist->playlist_name)}}</a> <br>
                            </div>
                            <div class="playlist-date">
                        <a href="" >{{ucwords($playlist->playlist_date)}}</a> <br>
                            </div>
                        </div>


{{--                                            </ul>--}}
                                        @endforeach
{{--                                    </div>--}}

        <table>
            <td><a href="{{route('frontend.front.song')}}" class="btn btn-info" > <i class="fa fa-plus"></i>Add Playlists</a></td>
        </table>

    </div>


</section>
<style>
    .playlist-date{
        font-size: 12px;
    }
    .playlist-list{
        margin-bottom: 10px;
    }
    .playlist-name{
        font-size: 20px;
    }
</style>
</br>
@include('frontend.includes.footer')
