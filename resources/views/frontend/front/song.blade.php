@include('frontend.includes.header')

<header id="header">
    <div class="header-content clearfix"> <span class="logo"><a href="index.html">Musicfy</a></span>
        @include('frontend.includes.menu')
        <a href="#" class="nav-toggle">Menu<span></span></a> </div>
</header>

<div class="abc">
    <img src="{{asset('demos/frontend/images/song.jpg')}}" alt="album" class="albumbladephp">
</div>

<section>
    <div class="container">
        <div class="section-header">
            <h1 class="wow fadeInDown animated">Our Songs</h1>
            <p class="wow fadeInDown animated">You can find different kind of genre....<br>You can select the songs that you like.</p>
        </div>

        <table>

            <td> <a href="" class="btn btn-info" ><i class="fa fa-plus"></i>Select Songs</a>
               </td>

        </table>
        </br>


        <div class="row">
            @foreach($songs as $song)
                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            <ul class="list-default">
                                <li>
                                    <a href="#" class="sf-with-ul">{{ucwords($song->song_name)}}</a>
                                </li>
                            </ul>
                        </label>
                    </div>
                </div>
                @endforeach
            </div>
        <a href="{{route('frontend.front.playlist')}}" class="btn btn-success" >New playlist</a>
    </div>
</section>
{{--<div class="container newsletter-popup-container mfp-hide" id="newsletter-popup-form">--}}
{{--<table class="table table-striped table-bordered" id="attribute_wrapper">--}}
{{--@if(isset($playlist))--}}
{{--    @foreach($playlists as $playlist)--}}
{{--        <tr>--}}
{{--            <td>--}}
{{--                {!! Form::select('$playlist_name[]',$data['$playlists'],$playlist->$playlist_name,['class' => 'form-control','placeholder' => "Select Playlists"]) !!}--}}
{{--            </td>--}}
{{--            <td>--}}

{{--            </td>--}}
{{--        </tr>--}}
{{--    @endforeach--}}
{{--@endif--}}
{{--</table>--}}
{{--</div>--}}


@include('frontend.includes.footer')
