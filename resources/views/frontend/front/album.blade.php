@include('frontend.includes.header')

<header id="header">
    <div class="header-content clearfix"> <span class="logo"><a href="index.html">Musicfy</a></span>
        @include('frontend.includes.menu')
        <a href="#" class="nav-toggle">Menu<span></span></a> </div>
</header>

<div class="abc">
    <img src="{{asset('demos/frontend/images/albums.jpg')}}" alt="album" class="albumbladephp">
</div>

<section>
    <div class="container">
        <div class="section-header">
            <h1 class="wow fadeInDown animated">Our Albums</h1>
            <p class="wow fadeInDown animated">They are the top most rated album where you can find different kinds of music <br> Also you can select the songs of your choice.</p>
        </div>
        <div class="row">
            <div class="col-md-6">

                <ul class="list-default">
                @foreach($albums as $album)

                        <li>
                            <a href="#" class="sf-with-ul">{{ucwords($album->name)}}</a>
                            </br>
                        </li>

                @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>

@include('frontend.includes.footer')
