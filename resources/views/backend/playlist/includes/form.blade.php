<div class="form-group">
    {!! Form::label('user_id', 'User'); !!}
    {!! Form::select('user_id',$data['users'],null, ['class' => 'form-control','placeholder' => 'Select User']); !!}
    @include('backend.includes.validation_message',['field' => 'user_id'])
</div>
<div class="form-group">
    {!! Form::label('playlist_name', ' playlist_name'); !!}
    {!! Form::text('playlist_name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'playlist_name'])
</div>
<div class="form-group">
{!! Form::label('playlist_date', ' playlist_date'); !!}
{!! Form::date('playlist_date',null, ['class' => 'form-control']); !!}
@include('backend.includes.validation_message',['field' => 'playlist_date'])
</div>
<div class="form-group">
    {!! Form::label('duration', ' duration'); !!}
    {!! Form::text('duration',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'duration'])
</div>


<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
