@extends('backend.layouts.backend')
@section('title','Trash ' . $panel)
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{$panel}} Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                        <li class="breadcrumb-item" ><a href="{{route($base_route . 'index')}}">{{$panel}}</a></li>
                        <li class="breadcrumb-item active">List</li>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List {{$panel}}
                    <a href="{{route($base_route . 'index')}}" class="btn btn-info">List</a>

                </h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">{{session('error')}}</div>
                @endif
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>User</th>
                        <th>Playlist_Name</th>
                        <th>Playlist_date</th>
                        <th>Duration</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($data['records'] as $record)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>@if($record->user_id)
                                    {{$record->user->name}}
                                @endif</td>
                            <td>{{$record->playlist_name}}</td>
                            <th>{{$record->playlist_date}}</th>
                            <th>{{$record->duration}}</th>
                            <td>{{$record->created_at}}</td>
                            <td>
                                <a href="{{route($base_route . 'restore',$record->id)}}" class="btn btn-info">Restore</a>
                                {!! Form::open(['route' => [$base_route .'force_delete', $record->id],'method' => 'delete']) !!}
                                {!! Form::submit('Force Delete',['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>
    <!-- /.content -->
@endsection
