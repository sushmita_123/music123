<div class="form-group">
    {!! Form::label('name', ' name'); !!}
    {!! Form::text('name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('email', ' email'); !!}
    {!! Form::email('email',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'email'])
</div>
<div class="form-group">
    {!! Form::label('message', ' message'); !!}
    {!! Form::text('message',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'message'])
</div>

<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
