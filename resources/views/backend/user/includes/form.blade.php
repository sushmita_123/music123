
<div class="form-group">
    {!! Form::label('name', 'Name'); !!}
    {!! Form::text('name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('email', 'Email'); !!}
    {!! Form::text('email',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'email'])
</div>
<div class="form-group">
    {!! Form::label('password', 'Password'); !!}
    {!! Form::password('password', ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'password'])
</div>
<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
