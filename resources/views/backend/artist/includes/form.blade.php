<div class="form-group">
    {!! Form::label('song_id', 'Song'); !!}
    {!! Form::select('song_id',$data['songs'],null, ['class' => 'form-control','placeholder' => 'Select Song','multiple' => 'multiple']); !!}
    @include('backend.includes.validation_message',['field' => 'song_id'])
</div>


<div class="form-group">
    {!! Form::label('artist_name', ' artist_name'); !!}
    {!! Form::text('artist_name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'artist_name'])
</div>

<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
