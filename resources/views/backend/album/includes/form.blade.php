<div class="form-group">
    {!! Form::label('song_id', 'Song'); !!}
    {!! Form::select('song_id',$data['songs'],null, ['class' => 'form-control','placeholder' => 'Select Song','multiple' => 'multiple']); !!}
    @include('backend.includes.validation_message',['field' => 'song_id'])
</div>
<div class="form-group">
    {!! Form::label('name', ' name'); !!}
    {!! Form::text('name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('genre', ' genre'); !!}
    {!! Form::text('genre',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'genre'])
</div>
<div class="form-group">
    {!! Form::label('year', ' year'); !!}
    {!! Form::date('year',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'year'])
</div>

<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
