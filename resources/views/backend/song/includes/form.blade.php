<div class="form-group">
    {!! Form::label('song_name', ' song_name'); !!}
    {!! Form::text('song_name',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'song_name'])
</div>
<div class="form-group">
{!! Form::label('title', ' title'); !!}
{!! Form::text('title',null, ['class' => 'form-control']); !!}
@include('backend.includes.validation_message',['field' => 'title'])
</div>
<div class="form-group">
    {!! Form::label('duration', ' duration'); !!}
    {!! Form::text('duration',null, ['class' => 'form-control']); !!}
    @include('backend.includes.validation_message',['field' => 'duration'])
</div>


<div class="form-group">
    {!! Form::submit($button,['class' => 'btn btn-success']) !!}
    {!! Form::button('Reset',['type' => 'reset','class' => 'btn btn-danger']); !!}
</div>
