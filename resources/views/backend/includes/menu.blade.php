<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
                <i class="fas fa-home"></i>
                <p>
                   Home
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.album.index')}}" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Album
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.artist.index')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Artist
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.song.index')}}" class="nav-link">
                <i class="nav-icon fas fa-list"></i>
                <p>
                    Song
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('backend.playlist.index')}}" class="nav-link">
                <i class="fas fa-cart-arrow-down"></i>
                <p>
                   Playlist
                </p>
            </a>
        </li>

        <li class="nav-header">User Management</li>
        <li class="nav-item">
            <a href="{{route('backend.user.index')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Users
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>
                    Logout
                </p>
            </a>
        </li>
    </ul>
</nav>
<!-- /.sidebar-menu -->

