@extends('backend.layouts.backend')
@section('main-content')

<section class="content-header">


   <div>

            <div class="image">
                <img src="{{asset('demos/backend/dist/img/user.jpg')}}" class="img-circle elevation-2" alt="User Image" >
            </div>

            <div class="info">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                    <th>Name</th>
                    <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{Auth::user()->name}}</td>
                        <td>{{Auth::user()->email}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
</section>
@endsection
